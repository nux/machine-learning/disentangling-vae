#!/usr/bin/env zsh

# Simple grid search
logger="train_all.out"
loss="betaB"
disentangledmetric="--betaB-G=""$beta"
resultsdir="/scratch/jzimmermann/disentangling-vae/"
echo "STARTING" >$logger
alias rm="rm"
for dataset in staticheliumnoround staticheliumnoroundcartcart staticheliumnoroundcart; do
  echo " " >>$logger
  echo $loss >>$logger
  for beta in 5 25 50 100 250 500; do
    echo $beta >>$logger
    dirname="$loss"_"$dataset"_"$disentangledmetric"
    dirname_nw="$(echo -e "${dirname}" | tr -d '[:space:]')"
    python main.py "$dirname_nw" -x "$loss"_"$dataset" "$disentangledmetric" --no-progress-bar
    python main_viz.py "$dirname_nw" all -c 10 -r 10 -t 2 --is-posterior
  done
done
python ./utils/plot_loss_vals.py
