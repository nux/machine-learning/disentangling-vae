import os
import glob
import pandas as pd
import matplotlib.pyplot as plt

path_to_models = "/scratch/jzimmermann/disentangling-vae"

train_loss_files = glob.glob(os.path.join(path_to_models, "**", "train_losses.log"),
                             recursive=True)
for f in train_loss_files:
    print(os.path.dirname(f))
    df = pd.pivot(pd.read_csv(f), index="Epoch", columns="Loss", values="Value")
    plt.close("all")
    df.plot(subplots=True, figsize=(10, 50))
    plt.tight_layout()
    plt.savefig(os.path.join(os.path.dirname(f), "losses.pdf"), dpi=300)
plt.close("all")
