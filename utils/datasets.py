import os
import abc
import hashlib
import glob
import logging
import socket
import subprocess
import numpy as np
from PIL import Image
from tqdm import tqdm

import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms

DIR = os.path.abspath(os.path.dirname(__file__))
COLOUR_BLACK = 0
COLOUR_WHITE = 1
DATASETS_DICT = {"staticheliumnoround": "StaticHeliumNoRound",
                 "staticheliumnoroundcartcart": "StaticHeliumNoRoundCartCart",
                 "staticheliumnoroundcart": "StaticHeliumNoRoundCart"}
DATASETS = list(DATASETS_DICT.keys())


def get_dataset(dataset):
    """Return the correct dataset."""
    dataset = dataset.lower()
    try:
        # eval because stores name as string in order to put it at top of file
        return eval(DATASETS_DICT[dataset])
    except KeyError:
        raise ValueError("Unkown dataset: {}".format(dataset))


def get_img_size(dataset):
    """Return the correct image size."""
    return get_dataset(dataset).img_size


def get_background(dataset):
    """Return the image background color."""
    return get_dataset(dataset).background_color


def get_dataloaders(dataset, root=None, shuffle=True, pin_memory=True,
                    batch_size=128, logger=logging.getLogger(__name__), **kwargs):
    """A generic data loader

    Parameters
    ----------
    dataset : {"mnist", "fashion", "dsprites", "celeba", "chairs"}
        Name of the dataset to load

    root : str
        Path to the dataset root. If `None` uses the default one.

    kwargs :
        Additional arguments to `DataLoader`. Default values are modified.
    """
    pin_memory = pin_memory and torch.cuda.is_available  # only pin if GPU available
    Dataset = get_dataset(dataset)
    dataset = Dataset(logger=logger) if root is None else Dataset(root=root, logger=logger)
    return DataLoader(dataset,
                      batch_size=len(dataset) if batch_size == -1 else batch_size,
                      shuffle=shuffle,
                      pin_memory=pin_memory,
                      **kwargs)


class DisentangledDataset(Dataset, abc.ABC):
    """Base Class for disentangled VAE datasets.

    Parameters
    ----------
    root : string
        Root directory of dataset.

    transforms_list : list
        List of `torch.vision.transforms` to apply to the data when loading it.
    """

    def __init__(self, root, transforms_list=[], logger=logging.getLogger(__name__)):
        self.root = root
        self.train_data = os.path.join(root, type(self).files["train"])
        self.transforms = transforms.Compose(transforms_list)
        self.logger = logger

        if not os.path.isdir(root):
            self.logger.info("Downloading {} ...".format(str(type(self))))
            self.download()
            self.logger.info("Finished Downloading.")

    def __len__(self):
        return len(self.imgs)

    @abc.abstractmethod
    def __getitem__(self, idx):
        """Get the image of `idx`.

        Return
        ------
        sample : torch.Tensor
            Tensor in [0.,1.] of shape `img_size`.
        """
        pass

    @abc.abstractmethod
    def download(self):
        """Download the dataset. """
        pass


class StaticHeliumNoRound(DisentangledDataset):
    """
    Modified Helium Dataset from [1, 2].

    Balanced static helium data from the 2015 experiment at the LDM
    end station at FERMI FEL-1. The experimental details are described in
    Phys. Rev. Lett. 121, 255301; Langbehn et al (2018). In addition to
    the scattering data, the data file contains labels for a supervised
    machine learning task.

    This is the dataset from [2] without any round images. Labels are
    the same multiclass multilabel labels.

    Further, the images are scaled between 0 and 1 and have an original
    resolution of 256 x 256.
    Radius is in nm.

    Parameters
    ----------
    root : string
        Root directory of dataset. If this is empty or doesn't exist, then
        the dataset will be downloaded first.

    References
    ----------
    [1] Langbehn, B. et al. Three-Dimensional Shapes of Spinning Helium Nanodroplets.
            Phys. Rev. Lett. 121, 255301 (2018).
    [2] Zimmermann, J. et al. Deep neural networks for classifying complex
        features in diffraction images.
            Phys. Rev. E 99, 063309 (2019).
    [3] Zimmermann, J. C. Probing ultrafast electron dynamics in helium
        nanodroplets with deep learning assisted diffraction imaging.
            Technische Universität Berlin, 2021
            doi:10.14279/depositonce-11748
    """
    url = "https://share.phys.ethz.ch/~nux/datasets/static_helium_no_round_cart_all_meta.npz"
    urls = {"train": url}
    files = {"train": "static_helium_no_round_cart_all_meta.npz"}
    file_keys = ['images', 'labels', 'cart_images', 'photon_energy', 'bunch_id', 'radius']
    i_s = 64
    img_size = (1, i_s, i_s)
    background_color = COLOUR_WHITE
    if socket.gethostname() == "nux-noether":
        root_dir = "/scratch/jzimmermann/datasets"
    else:
        root_dir = os.path.join(DIR, '../data/static_helium_no_round')

    def __init__(self, root=root_dir, **kwargs):
        super().__init__(root, [transforms.ToTensor()], **kwargs)

        with np.load(os.path.join(self.root, self.files["train"])) as d:
            self.imgs = d[self.file_keys[0]]
            self.lbl = d[self.file_keys[1]]
            self.pe = d[self.file_keys[3]]
            self.bi = d[self.file_keys[4]]
            self.rads = d[self.file_keys[5]]

    def download(self):
        """Download the dataset."""
        save_path = os.path.join(self.root, self.files["train"])
        os.makedirs(self.root)
        subprocess.check_call(["curl", "-L", type(self).urls["train"],
                               "--output", save_path])

        hash_code = 'e0a1ac6ec7b497ffc4a76e3bac2f8369'
        assert hashlib.md5(open(save_path, 'rb').read()).hexdigest() == hash_code, \
            '{} file is corrupted.  Remove the file and try again.'.format(save_path)

    def __getitem__(self, idx):
        """Get the image of `idx`

        Return
        ------
        sample : torch.Tensor
            Tensor in [0.,1.] of shape `img_size`.

        placeholder :
            Placeholder value as their are no targets.
        """

        img = self.imgs[idx]
        # reshape to (C x H x W) and resize
        img = transforms.functional.resize(self.transforms(img),
                                           self.i_s).type(torch.FloatTensor)

        labels = self.lbl[idx]
        photon_energy = self.pe[idx]
        bunch_id = self.bi[idx]
        radius = self.rads[idx]

        ret_tpl = (img,
                   labels,
                   photon_energy,
                   bunch_id,
                   radius)

        return img, ret_tpl  # cartesian projection as input, polar form as target


class StaticHeliumNoRoundCart(DisentangledDataset):
    """
    Modified Helium Dataset from [1, 2].

    Balanced static helium data from the 2015 experiment at the LDM
    end station at FERMI FEL-1. The experimental details are described in
    Phys. Rev. Lett. 121, 255301; Langbehn et al (2018). In addition to
    the scattering data, the data file contains labels for a supervised
    machine learning task.

    This is the dataset from [2] without any round images. Labels are
    the same multiclass multilabel labels.

    Further, the images are scaled between 0 and 1 and have an original
    resolution of 222 x 222.

    In addition to the diffraction images, this dataset contains the
    cartesian projections for every image for the projection learning
    task.

    Parameters
    ----------
    root : string
        Root directory of dataset. If this is empty or doesn't exist, then
        the dataset will be downloaded first.

    References
    ----------
    [1] Langbehn, B. et al. Three-Dimensional Shapes of Spinning Helium Nanodroplets.
            Phys. Rev. Lett. 121, 255301 (2018).
    [2] Zimmermann, J. et al. Deep neural networks for classifying complex
        features in diffraction images.
            Phys. Rev. E 99, 063309 (2019).
    [3] Zimmermann, J. C. Probing ultrafast electron dynamics in helium
        nanodroplets with deep learning assisted diffraction imaging.
            Technische Universität Berlin, 2021
            doi:10.14279/depositonce-11748
    """
    url = "https://share.phys.ethz.ch/~nux/datasets/static_helium_no_round_cart_all_meta.npz"
    urls = {"train": url}
    files = {"train": "static_helium_no_round_cart_all_meta.npz"}
    file_keys = ['images', 'labels', 'cart_images', 'photon_energy', 'bunch_id', 'radius']
    i_s = 256
    img_size = (1, i_s, i_s)
    background_color = COLOUR_WHITE
    if socket.gethostname() == "nux-noether":
        root_dir = "/scratch/jzimmermann/datasets"
    else:
        root_dir = os.path.join(DIR, '../data/static_helium_no_round')

    def __init__(self, root=root_dir, **kwargs):
        super().__init__(root, [transforms.ToTensor()], **kwargs)

        with np.load(os.path.join(self.root, self.files["train"])) as d:
            self.imgs = d[self.file_keys[0]]
            self.lbl = d[self.file_keys[1]]
            self.cart_imgs = d[self.file_keys[2]]
            self.pe = d[self.file_keys[3]]
            self.bi = d[self.file_keys[4]]
            self.rads = d[self.file_keys[5]]

    def download(self):
        """Download the dataset."""
        save_path = os.path.join(self.root, self.files["train"])
        os.makedirs(self.root)
        subprocess.check_call(["curl", "-L", type(self).urls["train"],
                               "--output", save_path])

        hash_code = 'e0a1ac6ec7b497ffc4a76e3bac2f8369'
        assert hashlib.md5(open(save_path, 'rb').read()).hexdigest() == hash_code, \
            '{} file is corrupted.  Remove the file and try again.'.format(save_path)

    def __getitem__(self, idx):
        """Get the image of `idx`

        Return
        ------
        sample : torch.Tensor
            Tensor in [0.,1.] of shape `img_size`.

        placeholder :
            Placeholder value as their are no targets.
        """

        img = self.imgs[idx]
        # reshape to (C x H x W) and resize
        img = transforms.functional.resize(self.transforms(img),
                                           self.i_s).type(torch.FloatTensor)

        cart_img = self.cart_imgs[idx]
        # reshape to (C x H x W) and resize
        cart_img = transforms.functional.resize(self.transforms(cart_img),
                                                self.i_s).type(torch.FloatTensor)
        labels = self.lbl[idx]
        photon_energy = self.pe[idx]
        bunch_id = self.bi[idx]
        radius = self.rads[idx]

        ret_tpl = (img,
                   labels,
                   photon_energy,
                   bunch_id,
                   radius)

        return cart_img, ret_tpl  # cartesian projection as input, polar form as target


class StaticHeliumNoRoundCartCart(DisentangledDataset):
    """
    Modified Helium Dataset from [1, 2].

    Balanced static helium data from the 2015 experiment at the LDM
    end station at FERMI FEL-1. The experimental details are described in
    Phys. Rev. Lett. 121, 255301; Langbehn et al (2018). In addition to
    the scattering data, the data file contains labels for a supervised
    machine learning task.

    This is the dataset from [2] without any round images. Labels are
    the same multiclass multilabel labels.

    Further, the images are scaled between 0 and 1 and have an original
    resolution of 222 x 222.

    In addition to the diffraction images, this dataset contains the
    cartesian projections for every image for the projection learning
    task.

    Parameters
    ----------
    root : string
        Root directory of dataset. If this is empty or doesn't exist, then
        the dataset will be downloaded first.

    References
    ----------
    [1] Langbehn, B. et al. Three-Dimensional Shapes of Spinning Helium Nanodroplets.
            Phys. Rev. Lett. 121, 255301 (2018).
    [2] Zimmermann, J. et al. Deep neural networks for classifying complex
        features in diffraction images.
            Phys. Rev. E 99, 063309 (2019).
    [3] Zimmermann, J. C. Probing ultrafast electron dynamics in helium
        nanodroplets with deep learning assisted diffraction imaging.
            Technische Universität Berlin, 2021
            doi:10.14279/depositonce-11748
    """
    url = "https://share.phys.ethz.ch/~nux/datasets/static_helium_no_round_cart_all_meta.npz"
    urls = {"train": url}
    files = {"train": "static_helium_no_round_cart_all_meta.npz"}
    file_keys = ['images', 'labels', 'cart_images', 'photon_energy', 'bunch_id', 'radius']
    i_s = 256
    img_size = (1, i_s, i_s)
    background_color = COLOUR_WHITE
    if socket.gethostname() == "nux-noether":
        root_dir = "/scratch/jzimmermann/datasets"
    else:
        root_dir = os.path.join(DIR, '../data/static_helium_no_round')

    def __init__(self, root=root_dir, **kwargs):
        super().__init__(root, [transforms.ToTensor()], **kwargs)

        with np.load(os.path.join(self.root, self.files["train"])) as d:
            self.imgs = d[self.file_keys[2]]
            self.lbl = d[self.file_keys[1]]
            self.pe = d[self.file_keys[3]]
            self.bi = d[self.file_keys[4]]
            self.rads = d[self.file_keys[5]]

    def download(self):
        """Download the dataset."""
        save_path = os.path.join(self.root, self.files["train"])
        os.makedirs(self.root)
        subprocess.check_call(["curl", "-L", type(self).urls["train"],
                               "--output", save_path])

        hash_code = 'e0a1ac6ec7b497ffc4a76e3bac2f8369'
        assert hashlib.md5(open(save_path, 'rb').read()).hexdigest() == hash_code, \
            '{} file is corrupted.  Remove the file and try again.'.format(save_path)

    def __getitem__(self, idx):
        """Get the image of `idx`

        Return
        ------
        sample : torch.Tensor
            Tensor in [0.,1.] of shape `img_size`.

        placeholder :
            Placeholder value as their are no targets.
        """

        cart_img = self.imgs[idx]
        # reshape to (C x H x W) and resize
        cart_img = transforms.functional.resize(self.transforms(cart_img),
                                                self.i_s).type(torch.FloatTensor)
        labels = self.lbl[idx]
        photon_energy = self.pe[idx]
        bunch_id = self.bi[idx]
        radius = self.rads[idx]

        ret_tpl = (cart_img,
                   labels,
                   photon_energy,
                   bunch_id,
                   radius)

        return cart_img, ret_tpl  # cartesian projection as input, polar form as target


# HELPERS
def preprocess(root, size=(64, 64), img_format='JPEG', center_crop=None):
    """Preprocess a folder of images.

    Parameters
    ----------
    root : string
        Root directory of all images.

    size : tuple of int
        Size (width, height) to rescale the images. If `None` don't rescale.

    img_format : string
        Format to save the image in. Possible formats:
        https://pillow.readthedocs.io/en/3.1.x/handbook/image-file-formats.html.

    center_crop : tuple of int
        Size (width, height) to center-crop the images. If `None` don't center-crop.
    """
    imgs = []
    for ext in [".png", ".jpg", ".jpeg"]:
        imgs += glob.glob(os.path.join(root, '*' + ext))

    for img_path in tqdm(imgs):
        img = Image.open(img_path)
        width, height = img.size

        if size is not None and width != size[1] or height != size[0]:
            img = img.resize(size, Image.ANTIALIAS)

        if center_crop is not None:
            new_width, new_height = center_crop
            left = (width - new_width) // 2
            top = (height - new_height) // 2
            right = (width + new_width) // 2
            bottom = (height + new_height) // 2

            img.crop((left, top, right, bottom))

        img.save(img_path, img_format)
