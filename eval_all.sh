#!/usr/bin/env zsh

# Simple grid search for five disentanglement architectures and their effective beta parameter
# See:
# Locatello, F. et al. Challenging common assumptions in the unsupervised learning of
# disentangled representations. 36th Int. Conf. Mach. Learn.
# ICML 2019 2019-June, 7247–7283 (2019).

logger="train_all.out"
dataset="balancedhelium"
echo "STARTING" >$logger
for loss in betaB factor btcvae betaH VAE; do
  echo " " >>$logger
  echo $loss >>$logger
  for beta in 1 2 4 8 10 14 18 22; do
    echo $beta >>$logger
    case $loss in
    "betaB") disentangledmetric="--betaB-G="$(expr $beta \* 250) ;;
    "factor") disentangledmetric="--factor-G="$(expr $beta \* 2) ;;
    "btcvae") disentangledmetric="--btcvae-B=""$beta" ;;
    "betaH") disentangledmetric="--betaH-B=""$beta" ;;
    "VAE") disentangledmetric="" ;;
    esac
    dirname="$loss"_"$dataset"_"$disentangledmetric"
    dirname_nw="$(echo -e "${dirname}" | tr -d '[:space:]')"
    python main_viz.py "$dirname_nw" all -c 10 -r 10 -t 0.45
    if [ $loss = "VAE" ]; then
      echo "We only do one run for the vanilla VAE"
      break
    fi
  done
done
