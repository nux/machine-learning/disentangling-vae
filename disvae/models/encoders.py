"""
Module containing the encoders.
"""
import numpy as np

import torch
from torch import nn


# ALL encoders should be called Enccoder<Model>
def get_encoder(model_type):
    model_type = model_type
    return eval("Encoder{}".format(model_type))


class EncoderBurgess(nn.Module):
    def __init__(self, img_size,
                 latent_dim=10, use_se=False):
        r"""Encoder of the model proposed in [1].

        Parameters
        ----------
        img_size : tuple of ints
            Size of images. E.g. (1, 32, 32) or (3, 64, 64).

        latent_dim : int
            Dimensionality of latent output.

        Model Architecture (transposed for decoder)
        ------------
        - 4 convolutional layers (each with 32 channels), (4 x 4 kernel), (stride of 2)
        - 2 fully connected layers (each of 256 units)
        - Latent distribution:
            - 1 fully connected layer of 20 units (log variance and mean for 10 Gaussians)

        References:
            [1] Burgess, Christopher P., et al. "Understanding disentangling in
            $\beta$-VAE." arXiv preprint arXiv:1804.03599 (2018).
        """
        super(EncoderBurgess, self).__init__()

        # Layer parameters
        hid_channels = 64
        kernel_size = 4
        hidden_dim = 256
        self.latent_dim = latent_dim
        self.img_size = img_size
        # Shape required to start transpose convs
        self.reshape = (hid_channels, kernel_size, kernel_size)
        n_chan = self.img_size[0]

        self.use_se = use_se
        if self.use_se:
            self.se_layer1 = SELayer(int(hid_channels // 2))
            self.se_layer2 = SELayer(int(hid_channels // 2))
            self.se_layer3 = SELayer(hid_channels)
            self.se_layer4 = SELayer(hid_channels)

        # Convolutional layers
        cnn_kwargs = dict(stride=2, padding=1)
        self.conv1 = nn.Conv2d(n_chan, int(hid_channels // 2),
                               (kernel_size, kernel_size), **cnn_kwargs)
        self.conv2 = nn.Conv2d(int(hid_channels // 2), int(hid_channels // 2),
                               (kernel_size, kernel_size), **cnn_kwargs)
        self.conv3 = nn.Conv2d(int(hid_channels // 2), hid_channels,
                               (kernel_size, kernel_size), **cnn_kwargs)

        # If input image is 64x64 do fourth convolution
        if self.img_size[1] == self.img_size[2] == 64:
            self.conv_64 = nn.Conv2d(hid_channels, hid_channels,
                                     (kernel_size, kernel_size), **cnn_kwargs)

        # Fully connected layers
        self.lin1 = nn.Linear(np.product(self.reshape), hidden_dim)
        self.lin2 = nn.Linear(hidden_dim, hidden_dim)

        # Fully connected layers for mean and variance
        self.mu_logvar_gen = nn.Linear(hidden_dim, self.latent_dim * 2)

    def forward(self, x):
        batch_size = x.size(0)

        # Convolutional layers with ReLu activations
        x = torch.relu(self.conv1(x))
        if self.use_se:
            x = self.se_layer1(x)
        x = torch.relu(self.conv2(x))
        if self.use_se:
            x = self.se_layer2(x)
        x = torch.relu(self.conv3(x))
        if self.use_se:
            x = self.se_layer3(x)
        if self.img_size[1] == self.img_size[2] == 64:
            x = torch.relu(self.conv_64(x))
            if self.use_se:
                x = self.se_layer4(x)

        # Fully connected layers with ReLu activations
        x = x.view((batch_size, -1))
        x = torch.relu(self.lin1(x))
        x = torch.relu(self.lin2(x))

        # Fully connected layer for log variance and mean
        # Log std-dev in paper (bear in mind)
        mu_logvar = self.mu_logvar_gen(x)
        mu, logvar = mu_logvar.view(-1, self.latent_dim, 2).unbind(-1)

        return mu, logvar


def conv3x3(in_planes: int,
            out_planes: int,
            stride: int = 1,
            groups: int = 1,
            dilation: int = 1) -> nn.Conv2d:
    """3x3 convolution with padding"""
    return nn.Conv2d(
        in_planes,
        out_planes,
        kernel_size=(3, 3),
        stride=(stride, stride),
        padding=dilation,
        groups=groups,
        bias=False,
        padding_mode="reflect",
        dilation=(dilation, dilation),
    )


def conv1x1(in_planes: int,
            out_planes: int,
            stride: int = 1) -> nn.Conv2d:
    """1x1 convolution"""
    return nn.Conv2d(in_planes, out_planes,
                     kernel_size=(1, 1),
                     stride=(stride, stride),
                     padding_mode="reflect",
                     bias=False)


class SELayer(nn.Module):
    def __init__(self, channel, reduction=16):
        super(SELayer, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Sequential(
            nn.Linear(channel, channel // reduction, bias=False),
            nn.ReLU(inplace=True),
            nn.Linear(channel // reduction, channel, bias=False),
            nn.Sigmoid()
        )

    def forward(self, x):
        b, c, _, _ = x.size()
        y = self.avg_pool(x).view(b, c)
        y = self.fc(y).view(b, c, 1, 1)
        return x * y.expand_as(x)


class BasicResNetBlock(nn.Module):
    expansion: int = 1

    def __init__(self, inplanes, planes, stride=1,
                 groups=1, base_width=64, dilation=1,
                 norm_layer=None, use_se=True):
        super().__init__()
        if norm_layer is None:
            norm_layer = nn.InstanceNorm2d
        if groups != 1 or base_width != 64:
            raise ValueError("BasicResNetBlock only supports groups=1 and base_width=64")
        if dilation > 1:
            raise NotImplementedError("Dilation > 1 not supported in BasicResNetBlock")
        # Both self.conv1 and self.downsample layers downsample the input when stride != 1
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.in1 = norm_layer(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.in2 = norm_layer(planes)
        self.downsample = nn.Sequential(
            conv1x1(inplanes, planes, stride),
            norm_layer(planes),
        )
        self.stride = stride
        self.use_se = use_se
        if self.use_se:
            self.se_layer = SELayer(planes)

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.in1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.in2(out)
        if self.use_se:
            out = self.se_layer(out)

        if self.stride > 1:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


class EncoderBurgessResNet(nn.Module):
    def __init__(self, img_size,
                 latent_dim=10):
        r"""Encoder of a modified model based on [1].
        Here we use a Basic Resnet Block for every conv layer

        Parameters
        ----------
        img_size : tuple of ints
            Size of images. E.g. (1, 32, 32) or (3, 64, 64).

        latent_dim : int
            Dimensionality of latent output.

        Model Architecture (transposed for decoder)
        ------------
        - 4 convolutional Basis ResNet blocks (each with 64/32 channels), (4 x 4 kernel), (stride of 2)
        - 2 fully connected layers (each of 256 units)
        - Latent distribution:
            - 1 fully connected layer of 20 units (log variance and mean for 10 Gaussians)

        """
        super(EncoderBurgessResNet, self).__init__()

        # Layer parameters
        hid_channels = 256
        hidden_dim = 256
        self.latent_dim = latent_dim
        self.img_size = img_size
        # Shape required to start transpose convs
        filter_shape = int(self.img_size[1] / 2 ** 5)
        self.reshape = (hid_channels, filter_shape, filter_shape)
        n_chan = self.img_size[0]

        # Convolutional layers
        self.conv1 = nn.Conv2d(n_chan, int(hid_channels // 8),
                               (7, 7), stride=(2, 2), padding=3,
                               padding_mode="reflect")
        self.conv2 = BasicResNetBlock(int(hid_channels // 8), int(hid_channels // 4), stride=2)
        self.conv3 = BasicResNetBlock(int(hid_channels // 4), int(hid_channels // 2), stride=2)
        self.conv4 = BasicResNetBlock(int(hid_channels // 2), hid_channels, stride=2)
        self.conv5 = BasicResNetBlock(hid_channels, hid_channels, stride=2)

        # Fully connected layers
        self.lin1 = nn.Linear(np.product(self.reshape), hidden_dim)
        self.lin2 = nn.Linear(hidden_dim, hidden_dim)

        # Fully connected layers for mean and variance
        self.mu_logvar_gen = nn.Linear(hidden_dim, self.latent_dim * 2)

    def forward(self, x):
        batch_size = x.size(0)

        # Convolutional layers with ReLu activations
        x = torch.relu(self.conv1(x))
        x = torch.relu(self.conv2(x))
        x = torch.relu(self.conv3(x))
        x = torch.relu(self.conv4(x))
        x = torch.relu(self.conv5(x))

        # Fully connected layers with ReLu activations
        x = x.view((batch_size, -1))
        x = torch.relu(self.lin1(x))
        x = torch.relu(self.lin2(x))

        # Fully connected layer for log variance and mean
        # Log std-dev in paper (bear in mind)
        mu_logvar = self.mu_logvar_gen(x)
        mu, logvar = mu_logvar.view(-1, self.latent_dim, 2).unbind(-1)

        return mu, logvar
