"""
Module containing the decoders.
"""
import numpy as np

import torch
from torch import nn


# ALL decoders should be called Decoder<Model>
def get_decoder(model_type):
    model_type = model_type
    # return DecoderBurgess
    return eval("Decoder{}".format(model_type))


class DecoderBurgess(nn.Module):
    def __init__(self, img_size,
                 latent_dim=10, use_se=True):
        r"""Decoder of the model proposed in [1].

        Parameters
        ----------
        img_size : tuple of ints
            Size of images. E.g. (1, 32, 32) or (3, 64, 64).

        latent_dim : int
            Dimensionality of latent output.

        Model Architecture (transposed for decoder)
        ------------
        - 4 convolutional layers (each with 32 channels), (4 x 4 kernel), (stride of 2)
        - 2 fully connected layers (each of 256 units)
        - Latent distribution:
            - 1 fully connected layer of 20 units (log variance and mean for 10 Gaussians)

        References:
            [1] Burgess, Christopher P., et al. "Understanding disentangling in
            $\beta$-VAE." arXiv preprint arXiv:1804.03599 (2018).
        """
        super(DecoderBurgess, self).__init__()

        # Layer parameters
        hid_channels = 64
        kernel_size = 4
        hidden_dim = 256
        self.img_size = img_size
        # Shape required to start transpose convs
        self.reshape = (hid_channels, kernel_size, kernel_size)
        n_chan = self.img_size[0]
        self.img_size = img_size

        self.use_se = use_se
        if self.use_se:
            self.se_layer1 = SELayer(hid_channels)
            self.se_layer2 = SELayer(int(hid_channels // 2))
            self.se_layer3 = SELayer(int(hid_channels // 2))
            self.se_layer4 = SELayer(int(hid_channels // 2))

        # Fully connected layers
        self.lin1 = nn.Linear(latent_dim, hidden_dim)
        self.lin2 = nn.Linear(hidden_dim, hidden_dim)
        self.lin3 = nn.Linear(hidden_dim, np.product(self.reshape))

        # Convolutional layers
        cnn_kwargs = dict(stride=2, padding=1)
        # If input image is 64x64 do fourth convolution
        if self.img_size[1] == self.img_size[2] == 64:
            self.convT_64 = nn.ConvTranspose2d(hid_channels, hid_channels,
                                               (kernel_size, kernel_size), **cnn_kwargs)

        self.convT1 = nn.ConvTranspose2d(hid_channels, int(hid_channels // 2),
                                         (kernel_size, kernel_size), **cnn_kwargs)
        self.convT2 = nn.ConvTranspose2d(int(hid_channels // 2), hid_channels,
                                         (kernel_size, kernel_size), **cnn_kwargs)
        self.convT3 = nn.ConvTranspose2d(hid_channels, n_chan,
                                         (kernel_size, kernel_size), **cnn_kwargs)

    def forward(self, z):
        batch_size = z.size(0)

        # Fully connected layers with ReLu activations
        x = torch.relu(self.lin1(z))
        x = torch.relu(self.lin2(x))
        x = torch.relu(self.lin3(x))
        x = x.view(batch_size, *self.reshape)

        # Convolutional layers with ReLu activations
        if self.img_size[1] == self.img_size[2] == 64:
            x = torch.relu(self.convT_64(x))
        x = torch.relu(self.convT1(x))
        x = torch.relu(self.convT2(x))
        # Sigmoid activation for final conv layer
        x = torch.sigmoid(self.convT3(x))

        return x


def conv_trans3x3(in_planes: int,
                  out_planes: int,
                  stride: int = 1,
                  groups: int = 1,
                  padding: int = 1,
                  output_padding: int = 0) -> nn.ConvTranspose2d:
    """3x3 convolution with padding"""
    return nn.ConvTranspose2d(
        in_planes,
        out_planes,
        kernel_size=(3, 3),
        stride=(stride, stride),
        padding=(padding, padding),
        groups=groups,
        bias=False,
        output_padding=(output_padding, output_padding)
    )


def conv3x3(in_planes: int,
            out_planes: int,
            stride: int = 1,
            groups: int = 1,
            dilation: int = 1) -> nn.Conv2d:
    """3x3 convolution with padding"""
    return nn.Conv2d(
        in_planes,
        out_planes,
        kernel_size=(3, 3),
        stride=(stride, stride),
        padding=dilation,
        groups=groups,
        bias=False,
        padding_mode="reflect",
        dilation=(dilation, dilation),
    )


def conv_trans1x1(in_planes: int,
                  out_planes: int,
                  stride: int = 1) -> nn.ConvTranspose2d:
    """1x1 convolution"""
    return nn.ConvTranspose2d(in_planes, out_planes,
                              kernel_size=(1, 1),
                              stride=(stride, stride),
                              bias=False,
                              output_padding=(1, 1))


class BasicResNetBlock(nn.Module):
    expansion: int = 1

    def __init__(self, inplanes, planes, stride=1,
                 groups=1, base_width=64, padding=1,
                 dilation=1, norm_layer=None, use_se=True
                 ) -> None:
        super().__init__()
        if norm_layer is None:
            norm_layer = nn.InstanceNorm2d

        if groups != 1 or base_width != 64:
            raise ValueError("BasicResNetBlock only supports groups=1 and base_width=64")
        if dilation > 1:
            raise NotImplementedError("Dilation > 1 not supported in BasicResNetBlock")

        # Both self.conv1 and self.downsample layers upample the input when stride != 1
        self.conv1 = conv_trans3x3(inplanes, planes, stride, output_padding=1)
        self.in1 = norm_layer(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.in2 = norm_layer(planes)
        self.upsample = nn.Sequential(
            conv_trans1x1(inplanes, planes, stride),
            norm_layer(planes),
        )
        self.stride = stride
        self.use_se = use_se
        if self.use_se:
            self.se_layer = SELayer(planes)

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.in1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.in2(out)
        if self.use_se:
            out = self.se_layer(out)

        if self.stride > 1:
            identity = self.upsample(x)

        out += identity
        out = self.relu(out)

        return out


class SELayer(nn.Module):
    def __init__(self, channel, reduction=16):
        super(SELayer, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Sequential(
            nn.Linear(channel, channel // reduction, bias=False),
            nn.ReLU(inplace=True),
            nn.Linear(channel // reduction, channel, bias=False),
            nn.Sigmoid()
        )

    def forward(self, x):
        b, c, _, _ = x.size()
        y = self.avg_pool(x).view(b, c)
        y = self.fc(y).view(b, c, 1, 1)
        return x * y.expand_as(x)


class DecoderBurgessResNet(nn.Module):
    def __init__(self, img_size,
                 latent_dim=10):
        r"""Decoder of a modified model based on [1].
        Here we use a Basic Resnet Block for every conv layer

        Parameters
        ----------
        img_size : tuple of ints
            Size of images. E.g. (1, 32, 32) or (3, 64, 64).

        latent_dim : int
            Dimensionality of latent output.

        Model Architecture (transposed for decoder)
        ------------
        - 4 convolutional Basis ResNet blocks (each with 64/32 channels), (4 x 4 kernel), (stride of 2)
        - 2 fully connected layers (each of 256 units)
        - Latent distribution:
            - 1 fully connected layer of 20 units (log variance and mean for 10 Gaussians)

        """
        super(DecoderBurgessResNet, self).__init__()

        # Layer parameters
        hid_channels = 256
        hidden_dim = 256
        self.img_size = img_size
        # Shape required to start transpose convs
        filter_shape = int(self.img_size[1] / 2 ** 5)
        self.reshape = (hid_channels, filter_shape, filter_shape)
        n_chan = self.img_size[0]
        self.img_size = img_size

        # Fully connected layers
        self.lin1 = nn.Linear(latent_dim, hidden_dim)
        self.lin2 = nn.Linear(hidden_dim, hidden_dim)
        self.lin3 = nn.Linear(hidden_dim, np.product(self.reshape))

        # Convolutional layers
        self.convT1 = BasicResNetBlock(hid_channels, hid_channels, stride=2)
        self.convT2 = BasicResNetBlock(hid_channels, int(hid_channels // 2), stride=2)
        self.convT3 = BasicResNetBlock(int(hid_channels // 2), int(hid_channels // 4), stride=2)
        self.convT4 = BasicResNetBlock(int(hid_channels // 4), int(hid_channels // 8), stride=2)

        self.convout = nn.ConvTranspose2d(int(hid_channels // 8), n_chan,
                                          (7, 7), stride=(2, 2), padding=(3, 3),
                                          output_padding=(1, 1))

    def forward(self, z):
        batch_size = z.size(0)

        # Fully connected layers with ReLu activations
        x = torch.relu(self.lin1(z))
        x = torch.relu(self.lin2(x))
        x = torch.relu(self.lin3(x))
        x = x.view(batch_size, *self.reshape)

        # Convolutional layers with ReLu activations
        x = torch.relu(self.convT1(x))
        x = torch.relu(self.convT2(x))
        x = torch.relu(self.convT3(x))
        x = torch.relu(self.convT4(x))
        # Sigmoid activation for final conv layer
        x = torch.sigmoid(self.convout(x))

        return x
